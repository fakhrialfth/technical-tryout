function countUniqueValue (arr) {
    let uniqueCount = 0;
    for (let i = 0; i < arr.length; i++) {
        if(arr[i] !== arr[i+1]){
            uniqueCount++;
        }
    }
    return uniqueCount;
}

console.log(countUniqueValue([1, 2, 2, 2, 2, 2, 2, 2, 4, 6]));